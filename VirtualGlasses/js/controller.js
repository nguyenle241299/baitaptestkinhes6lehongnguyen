export let renderGlassList = (list) => {
    let contentHTML = "";
    list.forEach((item) => {
        contentHTML += `  
        <span class = "p-2 col-4">
            <img id = "${item.id}"  class = "vglasses__items w-100" src="${item.src}"/>
        </span>    
        `
    });
    document.getElementById("vglassesList").innerHTML = contentHTML;
}

export let showGlassesOnAvarta = (data) => {
    let glassesList = document.querySelectorAll(".vglasses__items");
    for (const glasses of glassesList) {
        glasses.addEventListener("click", () => {  
          let index = data.find((item) => item.id == glasses.id);

          if (index) {
            contentAvarta(index);
    
            document.getElementById("glassesInfo").classList.add("d-block");
    
            glassesInfo(index);
          }
          document.getElementById("avatar").innerHTML = contentAvarta(index);
          document.getElementById("glassesInfo").innerHTML =glassesInfo(index);
        });
      }
    
}

let contentAvarta = (data) => {
    return `<img id="${data.id}" src="${data.virtualImg}" class="w-50" ></img>`;
}

let glassesInfo = (data) => {
    return `
    <h5>${data.name} - ${data.brand} (${data.color})</h5>
    <span class="bg-danger p-1 rounded">$${data.price}</span> <span class="text-success ml-1"> Stocking</span>
    <p class="mt-3">${data.description}</p>
    `;
};
  